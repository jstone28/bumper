build:
	go build -o bin

run:
	go run main.go

docker-build:
	docker build -t registry.gitlab.com/jstone28/bumper .

docker-push:
	docker push registry.gitlab.com/jstone28/bumper

deploy:
	kubectl apply -k manifests

port-forward:
	kubectl port-forward
